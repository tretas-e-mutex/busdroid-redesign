angular
  .module('busdroid')
  .service('linhaService', linhaService);

function linhaService($q) {

  const linhas = [
    {
      id: 1,
      nome: 'UFSC Semidireto Saída Norte'
    },
    {
      id: 2,
      nome: 'UFSC Semidireto Saída Sul'
    },
    {
      id: 3,
      nome: 'Canasvieiras - Trindade via UFSC'
    },
    {
      id: 4,
      nome: 'Canasvieiras - Direto'
    },
    {
      id: 5,
      nome: 'Pantanal Norte'
    },
    {
      id: 6,
      nome: 'Pantanal Sul'
    }
  ];

  this.findAll = function() {
    var deferred = $q.defer();
    deferred.resolve(angular.copy(linhas));
    return deferred.promise;
  };

  this.findOne = function(id) {
    var deferred = $q.defer();
    for(var i in linhas) {
      if(linhas[i].id === id) {
        deferred.resolve(angular.copy(linhas[i]));
        break;
      }
    }
    deferred.resolve(null);
    return deferred.promise;
  };

  this.findSchedules = function(ids) {
    var deferred = $q.defer();
    var horarios = [];
    for(var i in linhas) {
      var linha = linhas[i];
      if(ids.indexOf(linha.id) !== -1) {
        for(var j in linha.horarios) {
          var horario = linha.horarios[j];
          var data = new Date();
          data.setHours(horario.horas);
          data.setMinutes(horario.minutos);
          horarios.push({
            linha: linha.nome,
            horario: data
          });
        }
      }
    }
    deferred.resolve(horarios.sort(function(h1, h2) {
      return h1.horario.getTime() - h2.horario.getTime();
    }));
    return deferred.promise;
  };

  // Inicializa horarios
  for(var i in linhas) {
    var linha = linhas[i];
    linha.horarios = [];
    for(var j = 0; j < 24; j++) {
      if(linha.id % 2 === 0) {
        linha.horarios = linha.horarios.concat([{horas: j, minutos: 0}, {horas: j, minutos: 15}, {horas: j, minutos: 30}, {horas: j, minutos: 45}]);
      } else {
        linha.horarios = linha.horarios.concat([{horas: j, minutos: 0}, {horas: j, minutos: 30}]);
      }
    }
  }

}
