angular
  .module('busdroid')
  .service('listaService', listaService);

function listaService($q) {

  const listas = [
    {
      id: 1,
      nome: 'Casa - UFSC',
      linhas: [1, 2, 3]
    },
    {
      id: 2,
      nome: 'Trabalho - UFSC',
      linhas: [3, 6]
    },
    {
      id: 3,
      nome: 'Casa - Trabalho',
      linhas: [2, 3, 4, 5]
    }
  ];

  this.findOne = function(id) {
    var deferred = $q.defer();
    angular.forEach(listas, function(lista) {
      if(id === lista.id) {
        deferred.resolve(angular.copy(lista));
        return false;
      }
    });
    return deferred.promise;
  };

  this.findAll = function() {
    var deferred = $q.defer();
    deferred.resolve(angular.copy(listas));
    return deferred.promise;
  };

  this.save = function(lista) {
    var deferred = $q.defer();
    if(lista.id) {
      var idx = -1;
      angular.forEach(listas, function(l, i) {
        if(lista.id === l.id) {
          idx = i;
          return false;
        }
      });
      listas[idx] = lista;
    } else {
      var maxId = -1;
      angular.forEach(listas, function(lista) {
        if(lista.id > maxId)
          maxId = lista.id;
      });
      lista.id = maxId + 1;
      listas.push(lista);
    }
    deferred.resolve(lista.id);
    return deferred.promise;
  };

  this.remove = function(id) {
    var deferred = $q.defer();
    var idx = -1;
    angular.forEach(listas, function(lista, i) {
      if(id === lista.id) {
        idx = i;
        return false;
      }
    });
    var deletada = listas[idx];
    listas.splice(idx, 1);
    deferred.resolve(deletada);
    return deferred.promise;
  };

}
