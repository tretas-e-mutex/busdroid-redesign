angular
  .module('busdroid')
  .controller('HomeController', HomeController);

function HomeController($scope, $state, $stateParams, $ionicPopup, ionicMaterialInk, listaService, linhaService) {

  $scope.createList = function() {
    $ionicPopup.show({
      template: '<input type="text" ng-model="novaLista.nome" required>',
      title: 'Nova Lista',
      scope: $scope,
      buttons: [
        {
          text: 'Cancelar',
          type: 'button-default button-raised',
          onTap: function() {
            $scope.novaLista = {};
          }
        },
        {
          text: 'Ok',
          type: 'button-assertive button-raised',
          onTap: function(e) {
            if(!$scope.novaLista.nome.trim()) {
              e.preventDefault();
            } else {
              $state.go('editList', {lista: $scope.novaLista});
            }
          }
        }
      ]
    });
  };

  $scope.view = $stateParams.view;
  $scope.novaLista = {};

  listaService
    .findAll()
    .then(function(data) {
      $scope.listas = data;
    });

  linhaService
    .findAll()
    .then(function(data) {
      $scope.linhas = data;
    });

  ionicMaterialInk.displayEffect();

}
