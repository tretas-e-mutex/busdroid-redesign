angular
  .module('busdroid')
  .controller('EditarListaController', EditarListaController);

function EditarListaController($scope, $state, $stateParams, $ionicPopup, ionicMaterialInk, listaService, linhaService) {

  $scope.save = function() {
    if($scope.lista.nome) {
      var linhas = [];
      angular.forEach($scope.linhas, function(linha) {
        if(linha.selected) {
          linhas.push(linha.id);
        }
      });
      if(linhas.length) {
        $scope.lista.linhas = linhas;
        listaService
          .save($scope.lista)
          .then(function(id) {
            $state.go('list', {id: id});
          });
      } else {
        $ionicPopup.alert({
          title: 'Erro!',
          template: 'Ao menos uma linha deve ser selecionada!'
        });
      }
    } else {
      $ionicPopup.alert({
        title: 'Erro!',
        template: 'A lista deve ter um nome!'
      });
    }
  };

  $scope.lista = $stateParams.lista;

  linhaService
    .findAll()
    .then(function(data) {
      outer: for(var i in data) {
        data[i].selected = false;
        for(var j in $scope.lista.linhas) {
          if(data[i].id === $scope.lista.linhas[j]) {
            data[i].selected = true;
            continue outer;
          }
        }
      }
      $scope.linhas = data;
    });

  ionicMaterialInk.displayEffect();

}
