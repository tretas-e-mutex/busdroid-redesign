angular
  .module('busdroid')
  .controller('ListaController', ListaController);

function ListaController($scope, $state, $stateParams, $ionicPopup, $ionicPopover, ionicMaterialInk, listaService, linhaService) {

  $scope.showPopover = function($event) {
    $scope.popover.show($event);
  };

  $scope.editList = function() {
    $scope.popover.hide();
    $state.go('editList', {lista: $scope.lista});
  };

  $scope.promptRemoveList = function() {
    $scope.popover.hide();
    $ionicPopup.confirm({
      title: 'Excluir Lista',
      template: 'Deseja realmente excluir esta lista?',
      cancelText: 'Não',
      cancelType: 'button-default buton-raised',
      okText: 'Sim',
      okType: 'button-assertive button-raised'
    }).then(function(res) {
      if(res) {
        listaService
          .remove($scope.lista.id)
          .then(function() {
            $state.go('home');
          });
      }
    });
  };

  listaService
    .findOne($stateParams.id)
    .then(function(data) {
      $scope.lista = data;
      linhaService
        .findSchedules($scope.lista.linhas)
        .then(function(horarios) {
          $scope.horarios = horarios;
        });
    });

  $ionicPopover
    .fromTemplateUrl('list-popover.tpl.html', {scope: $scope})
    .then(function(popover) {
      $scope.popover = popover;
    });
  $scope.$on('destroy', function() {
    $scope.popover.remove();
  });

  ionicMaterialInk.displayEffect();

}
