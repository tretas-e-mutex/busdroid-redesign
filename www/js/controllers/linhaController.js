angular
  .module('busdroid')
  .controller('LinhaController', LinhaController);

function LinhaController($scope, $stateParams, ionicMaterialInk, linhaService) {

  linhaService
    .findOne($stateParams.id)
    .then(function(data) {
      $scope.linha = data;
    });

  linhaService
    .findSchedules([$stateParams.id])
    .then(function(data) {
      $scope.horarios = data;
    });

  ionicMaterialInk.displayEffect();

}
