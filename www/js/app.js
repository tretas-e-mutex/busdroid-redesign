angular
  .module('busdroid', ['ionic', 'ionic-material'])
  .run(function($rootScope, $state, $ionicPlatform) {

    $ionicPlatform.ready(function() {

      if(window.AndroidFullScreen) {
        AndroidFullScreen.immersiveMode();
      }

      if(window.cordova && window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }

      if(window.StatusBar) {
        StatusBar.styleDefault();
      }

    });

    $rootScope.$on('$stateChangeSuccess', function(e, to, toParams, from) {
      $rootScope.previousState = from.name;
    });

    $rootScope.backButtonHandler = null;
    $ionicPlatform.onHardwareBackButton(function() {
      ionic.Platform.exitApp();
    });

  })
  .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.views.maxCache(0);

    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'templates/home.html',
        controller: 'HomeController',
        params: {
          view: 'listas',
          onBackPressed: function($rootScope) {
            $rootScope.backButtonHandler = function() {
              ionic.Platform.exitApp();
            }
          }
        }
      })
      .state('editList', {
        url: '/editar-lista',
        templateUrl: 'templates/editar_lista.html',
        controller: 'EditarListaController',
        params: {
          lista: null,
          onBackPressed: function($state, $rootScope) {
            $rootScope.backButtonHandler = function() {
              if($rootScope.previousState === 'home') {
                $state.go('home', {view: 'listas'});
              } else {
                $state.go('list', {id: $state.params.lista.id});
              }
            }
          }
        }
      })
      .state('list', {
        url: '/lista',
        templateUrl: 'templates/lista.html',
        controller: 'ListaController',
        params: {
          id: 0,
          onBackPressed: function($state, $rootScope) {
            $rootScope.backButtonHandler = function() {
              $state.go('home', {view: 'listas'});
            }
          }
        }
      })
      .state('line', {
        url: '/linha',
        templateUrl: 'templates/linha.html',
        controller: 'LinhaController',
        params: {
          id: 0,
          onBackPressed: function($state, $rootScope) {
            $rootScope.backButtonHandler = function() {
              $state.go('home', {view: 'linhas'});
            }
          }
        }
      });

    $urlRouterProvider.otherwise('/home');

  });
